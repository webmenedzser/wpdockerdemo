<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'generateme' );

/** MySQL hostname */
define( 'DB_HOST', 'database' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'eME,6-@hn7jq`xK8bm3;!$E9j|Qu@0 r?pHis>I~4;e}|j6!yTPXw|_P;H&[v=K!' );
define( 'SECURE_AUTH_KEY',  'S<QN85Vt*IHl<g)oMm|{1ne?*Cb6h<#=_]nGkdIrs*1WZ5NmxOA1J7Jb&<^c9sI,' );
define( 'LOGGED_IN_KEY',    'stZFly$ne-|=@X_8rM[u%pK_K-@4X7`yEd^T/.;W;_nsk 3HNiT6OX|h0%Q%9ZIc' );
define( 'NONCE_KEY',        'Q {)j(83owv#tgaQpfHYew**d8;gPxS(m[Io>1``Tf9I%:!p6x;9P1YP23%o&6kb' );
define( 'AUTH_SALT',        'L/NiCyQ0kMLOL(Z/=>h9u:T4v`+EI+cQ^&Klmpv [I=3V_D[di{Ytd:<@U?4yb]T' );
define( 'SECURE_AUTH_SALT', 'z:ANZeT(hO ${;y7jhuc{<7?^vOylULNY*I_C[[5,^?1&ZO3]&w+mh3hzT9>?X<,' );
define( 'LOGGED_IN_SALT',   'J@?N=}Ui&8PDYFi@Fqh^+Q,>]*R%$E*h5_D,Z)Wzg_fZH<IDAl7OnfRsRA>1J)Fm' );
define( 'NONCE_SALT',       'Wmm[!kvd]B[pg:;KHxlbx?fgzP) A0n>e5vq$$m,bQ;phFGA>n%.8m!?1hURvZ`q' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
