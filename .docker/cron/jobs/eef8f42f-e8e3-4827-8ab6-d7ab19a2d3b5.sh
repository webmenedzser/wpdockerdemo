#!/usr/bin/env bash
set -e

# TODO find workaround
# [error] write /dev/stdout: broken pipe <- when using docker commands
#UUID=$(cat /proc/sys/kernel/random/uuid)
#exec > >(read message; echo "${UUID} $(date -Iseconds) [info] $message" | tee -a /var/log/crontab/jobs.log )
#exec 2> >(read message; echo "${UUID} $(date -Iseconds) [error] $message" | tee -a /var/log/crontab/jobs.log >&2)

echo "Start Cronjob **eef8f42f-e8e3-4827-8ab6-d7ab19a2d3b5** Run a cronjob"

echo 'Cron job is running.'
docker exec --user 1000:1000 wpdocker_php echo 'hello'
echo "End Cronjob **eef8f42f-e8e3-4827-8ab6-d7ab19a2d3b5** Run a cronjob"
