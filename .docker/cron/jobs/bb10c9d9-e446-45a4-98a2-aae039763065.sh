#!/usr/bin/env bash
set -e

# TODO find workaround
# [error] write /dev/stdout: broken pipe <- when using docker commands
#UUID=$(cat /proc/sys/kernel/random/uuid)
#exec > >(read message; echo "${UUID} $(date -Iseconds) [info] $message" | tee -a /var/log/crontab/jobs.log )
#exec 2> >(read message; echo "${UUID} $(date -Iseconds) [error] $message" | tee -a /var/log/crontab/jobs.log >&2)

echo "Start Cronjob **bb10c9d9-e446-45a4-98a2-aae039763065** Run a cronjob"

echo 'Cron job is running.'
docker exec --user 1000:1000 wpdocker_wpcli wp --user=cronuser --url=http://php civicrm api job.execute auth=0
echo "End Cronjob **bb10c9d9-e446-45a4-98a2-aae039763065** Run a cronjob"
