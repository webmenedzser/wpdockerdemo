#!/usr/bin/env bash
set -e

# TODO find workaround
# [error] write /dev/stdout: broken pipe <- when using docker commands
#UUID=$(cat /proc/sys/kernel/random/uuid)
#exec > >(read message; echo "${UUID} $(date -Iseconds) [info] $message" | tee -a /var/log/crontab/jobs.log )
#exec 2> >(read message; echo "${UUID} $(date -Iseconds) [error] $message" | tee -a /var/log/crontab/jobs.log >&2)

echo "Start Cronjob **257dfdad-dd7e-44a9-a6b2-6bd051bf808c** Run a cronjob"

echo 'Cron job is running.'
docker exec --user 1000:1000 wpdocker_php echo 'hello'
echo "End Cronjob **257dfdad-dd7e-44a9-a6b2-6bd051bf808c** Run a cronjob"
