#!/usr/bin/env sh

# Set variables
WEBSITE_VERSION=$(node -e "console.log(require('./package.json').version);")

echo '### Building Docker images ###'
docker-compose -f docker-compose-production.yml build --no-cache --pull --build-arg WEBSITE_VERSION=$WEBSITE_VERSION

echo "### Use the following version for build: $WEBSITE_VERSION ###"

echo '### Tagging Docker images ###'
docker tag wpdocker_web:latest registry.gitlab.com/webmenedzser/wpdockerdemo/web:$WEBSITE_VERSION
docker tag wpdocker_php:latest registry.gitlab.com/webmenedzser/wpdockerdemo/php:$WEBSITE_VERSION
docker tag wpdocker_redis:latest registry.gitlab.com/webmenedzser/wpdockerdemo/redis:$WEBSITE_VERSION
docker tag wpdocker_redis:latest registry.gitlab.com/webmenedzser/wpdockerdemo/cron:$WEBSITE_VERSION
docker tag wpdocker_redis:latest registry.gitlab.com/webmenedzser/wpdockerdemo/wpcli:$WEBSITE_VERSION

echo '### Pushing Docker images to Google Cloud ###'
docker push registry.gitlab.com/webmenedzser/wpdockerdemo/web:$WEBSITE_VERSION
docker push registry.gitlab.com/webmenedzser/wpdockerdemo/php:$WEBSITE_VERSION
docker push registry.gitlab.com/webmenedzser/wpdockerdemo/redis:$WEBSITE_VERSION
docker push registry.gitlab.com/webmenedzser/wpdockerdemo/cron:$WEBSITE_VERSION
docker push registry.gitlab.com/webmenedzser/wpdockerdemo/wpcli:$WEBSITE_VERSION
